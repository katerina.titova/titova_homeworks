package homework_05;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Реализовать программу на Java,которая для последовательности чисел,
 * оканчивающихся на -1 выведет самую минимальную цифру,
 * встречающуюся среди чисел последовательности
 * <p>
 * Например:
 * <p>
 * 345
 * 298
 * 456
 * -1
 * <p>
 * Ответ: 2
 */
class Homework5 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new StreamReader(System.in));
        int minDigit = 9;
        while (true) {
            var line:String = reader.readLine();
            int number = -1;
            try {
                number = Integer.parseInt(line);
            } catch (NumberFormatException e) {
                System.out.println("Введите, пожалуйста, число еще раз");
            }
            if (number == -1) {
                break;
            }
            if (number == 0) {
                minDigit = 0;
                continue;
            }
            while (number != 0) {
                var digit = number % 10;
                if (digit < minDigit) {
                    minDigit = digit;
                }
                number /= 10;
            }
        }
        System.out.println(minDigit);
    }
}
